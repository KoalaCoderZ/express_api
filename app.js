const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const app = express();
const port = process.env.port || 3001;
const useRoute = require('./Routes');
app.use(fileUpload());
app.use(cors({
    origin: '*',
}))

// Main Route
app.use('/', useRoute);

// Upload File
app.use('/upload', useRoute);

app.listen(port, ()=> console.log(`Listening on port ${port}`));
