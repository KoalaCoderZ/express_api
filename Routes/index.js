const express = require('express');
const router = express.Router();

const controller = require('../Controllers/HomeController');
const fileController = require('../Controllers/UploadController');
router.get('/', controller.home);
router.post('/upload', fileController.upload);

module.exports = router;