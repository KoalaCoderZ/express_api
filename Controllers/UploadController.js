const path = require('path');
module.exports = {
    upload : (req, res) => {
        let sampleFile;
        const baseUrl = path.join(__dirname, '../');
        let uploadPath;
        let username;
        // console.log(req.files);

        if(!req.files || Object.keys(req.files).length === 0) {
            return res.statusCode(400).send('No Files');
        }

        sampleFile = req.files.file;
        username = req.body.userName;        
        uploadPath = baseUrl + `upload\\` + sampleFile.name;
        console.log(uploadPath);

        // use mv() to place file on the server
        sampleFile.mv(uploadPath, function (err) {
            if(err) return res.status(500).send(err);
            const data = {
                'status': 200,
                'fileName': sampleFile.name,
                'userName': username
            }
            res.send(data);
        });
    },
}